using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPack : MonoBehaviour
{
    [SerializeField] private float exampleHealth;
    [SerializeField] private HealthController healthGain;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            heal();
        }
    }

    void heal()
    {
        healthGain.playerHealth = healthGain.playerHealth + exampleHealth;
        healthGain.UpdateHealth();
        this.gameObject.SetActive(false);
    }
}
