using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class VelocityClamp : MonoBehaviour
{

    //public CharacterController2D controller;
    public float maxSpeed = 10f;
    private Rigidbody2D player;

    private void Start()
    {
        player = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        // Trying to Limit Speed
        if (player.velocity.magnitude > maxSpeed)
        {
            player.velocity = Vector3.ClampMagnitude(player.velocity, maxSpeed);
        }
    }

    void OnGUI()
    {
        GUI.Label(new Rect(20, 20, 200, 200), "rigidbody velocity: " + player.velocity);
    }
}

