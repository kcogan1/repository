using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageController : MonoBehaviour
{
    [SerializeField] private float exampleDamage;
    [SerializeField] private HealthController health;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            damage();
        }
    }

    void damage()
    {
        health.playerHealth = health.playerHealth - exampleDamage;
        health.UpdateHealth();
        this.gameObject.SetActive(false);
    }
}
