using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour { 

    public CharacterController2D controller;
    public float runSpeed = 40f;

    float horizontalMove = 0f;
    bool jump = false;
    private Rigidbody2D rb;
    [SerializeField] private HealthController health;
    public float fallDamage = 1;
    private float maxYV;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        //get input
        horizontalMove = Input.GetAxisRaw("Horizontal") * runSpeed; 
            //Horizontal values:
            //Left or A = -1
            //Right or D - +1


        if (Input.GetButtonDown("Jump"))
        {
            jump = true;
        }
    }

    void FixedUpdate()
    {
        //impliment input
        controller.Move(horizontalMove * Time.fixedDeltaTime, false, jump);
        jump = false; //AHHHHH


        if (jump && rb.velocity.y == 0)
        {
            jump = false;

            if((maxYV <= -0))
            {
                doFallDamage();
                Debug.Log("going too fast");
            }
        }

        else if(jump)
        {
            if (rb.velocity.y < maxYV)
                maxYV = rb.velocity.y;
            Debug.Log("calculating max v");
        }

        if (Input.GetButtonDown("Jump") && !jump)
        {
            jump = true;
            maxYV = 0;
        }
    }

    public void doFallDamage()
    {
        health.playerHealth = health.playerHealth - fallDamage;
        health.UpdateHealth();
    }
}

