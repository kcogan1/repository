using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthController : MonoBehaviour
{
    public float playerHealth;
    [SerializeField] private Text healthValue;

    private void Start()
    {
        UpdateHealth();
    }

    public void UpdateHealth()
    {
        healthValue.text = playerHealth.ToString("0");
    }
}
