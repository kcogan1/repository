using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallDamage : MonoBehaviour
{
    [SerializeField] public float damageSpeed = -1;
    [SerializeField] private float fallDamage = 1;
    [SerializeField] private HealthController health;
    private Rigidbody2D rb;

    private void Start()
    {
       rb = GetComponent<Rigidbody2D>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Platforms") && rb.velocity.y < damageSpeed)
        {
            doFallDamage();
        }
    }

    void doFallDamage()
    {
        health.playerHealth = health.playerHealth - fallDamage;
        health.UpdateHealth();
        //this.gameObject.SetActive(false);
    }

}
